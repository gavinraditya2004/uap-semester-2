package Contest;

class VIP extends TiketKonser {
    // Do your magic here...
     public VIP(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}
