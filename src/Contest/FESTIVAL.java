package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...
     public FESTIVAL(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}
