package Contest;

class CAT1 extends TiketKonser {
    public CAT1(String nama, double harga) {
        super(nama, harga);
        //fungsinya adalah untuk memulai nama dan harga dari kelas CAT1
    }

    @Override
    public double hitungHarga() {
        return harga;
        //fungsinya adalah mengembalikan nilai dari harga awal
    }
}
    
    