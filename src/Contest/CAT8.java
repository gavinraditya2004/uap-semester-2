package Contest;

class CAT8 extends TiketKonser {
    //Do your magic here...
    public CAT8(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}