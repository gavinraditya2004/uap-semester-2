package Contest;

class PemesananTiket {
    // Do your magic here...
     private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 155000000),
            new CAT1("CAT 1", 85000000),
            new FESTIVAL("FESTIVAL", 78000000),
            new VIP("VIP", 9500000),
            new VVIP("UNLIMITED EXPERIENCE", 11500000)
        };
    }
//fungsinya untuk menyimpan data harga yang adalaah
    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
        //fungsi ini menjadi akses dan menjadikan tiket konser yang sesuai dari array
    }
}
